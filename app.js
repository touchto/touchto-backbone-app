(function() {
  (function(window, factory) {
    "use strict";
    var Backbone, Layout, _;
    if (typeof define === 'function' && define.amd) {
      define(['backbone', 'underscore', 'jquery', 'backbone.layoutmanager'], function() {
        return factory.apply(window, arguments);
      });
    } else if (typeof exports === 'object') {
      Backbone = require('backbone');
      _ = require('underscore');
      Layout = require('backbone.layoutmanager');
      Backbone.$ = require('jquery');
      module.exports = factory.call(window, Backbone, _, Backbone.$, Layout);
    } else {
      window.App = factory.call(window, window.Backbone, window._, window.Backbone.$);
    }
  })((typeof global === "object" ? global : this), function(Backbone, _, $) {
    "use strict";
    var App, action, action_selector, app, window;
    window = this;
    if (Function.prototype.define === void 0) {
      Function.prototype.define = function(prop, desc) {
        return Object.defineProperty(this.prototype, prop, desc);
      };
    }
    App = (function() {
      var _JST, _document, _name, _root, _title, _title_c, _title_template, setTitle;

      App.prototype.root_regex = new RegExp('', 'i');

      _root = null;

      _name = 'Untitled Application';

      _title = 'Untitled';

      _title_template = '<%= title %> | <%= name %>';

      _title_c = _.template(_title_template);

      _document = window.document !== void 0 ? window.document : {};

      _JST = {};

      function App() {
        if ('document' in window) {
          $(_document).on('ajaxError', function(event, xhr, options) {});
        }
        return this;
      }

      App.define('root', {
        get: function() {
          return _root;
        },
        set: function(val) {
          var root_escaped;
          _root = val;
          root_escaped = _root.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$&');
          this.root_regex = new RegExp("^(?!\/)(?!" + root_escaped + ")(.+)$", 'i');
          return _root;
        }
      });

      App.define('title', {
        get: function() {
          return _title;
        },
        set: function(val) {
          _title = val;
          setTitle();
          return _title;
        }
      });

      App.define('title_template', {
        get: function() {
          return _title_template;
        },
        set: function(val) {
          _title_template = val;
          _title_c = _.template(_title_template);
          setTitle();
          return _title_template;
        }
      });

      App.define('name', {
        get: function() {
          return _name;
        },
        set: function(val) {
          _name = val;
          setTitle();
          return _name;
        }
      });

      setTitle = function() {
        var title;
        title = _title_c({
          title: _title,
          name: _name
        });
        _document.title = unescape(title);
      };

      App.prototype.cache_bust = '?v=' + Math.random();

      App.prototype.module = function(additional_properties) {
        return _.extend({
          Routes: {},
          Controllers: {},
          Models: {},
          Collections: {},
          Views: {}
        }, additional_properties);
      };

      App.prototype.click = (typeof Modernizr !== "undefined" && Modernizr !== null ? Modernizr.touch : void 0) ? 'touchend' : 'click';

      App.prototype.useLayout = function(options) {
        var layout;
        options = _.extend({}, {
          el: 'body'
        }, options);
        layout = new Backbone.Layout(options);
        return this.layout = layout;
      };

      App.prototype.JST = _JST;

      return App;

    })();
    app = _.extend(new App(), Backbone.Events);
    if (app.root === null) {
      app.root = '/';
    }
    action = function(event) {
      var href, root, self;
      if (this.dragging) {
        delete this.dragging;
        return;
      }
      if ($(this).hasClass('dropdown-toggle')) {
        return true;
      }
      self = $(this);
      root = location.protocol + "//" + location.host + app.root;
      href = {
        prop: self.prop('href'),
        attr: self.attr('href').replace(root.substr(0, root.length - 1), '')
      };
      if (href.prop.slice(0, root.length) === root) {
        event.preventDefault && event.preventDefault();
        Backbone.history.navigate(href.attr, true);
        if ($(this).attr('role') === 'menuitem') {
          $('.dropdown-toggle', $(this).parents('.dropdown')).trigger('click');
          if ($(this).parents('.navbar-collapse.in').length > 0) {
            $('.navbar-toggle', $(this).parents('.navbar')).trigger('click');
          }
        }
        return false;
      }
    };
    if ('document' in window) {
      action_selector = 'a[href]:not([data-bypass]):not([href=\\#])';
      $(document).on(app.click, action_selector, action);
      if (app.click === 'touchend') {
        $(document).on('touchmove', action_selector, function(event) {
          this.dragging = true;
        });
      }
    }
    Backbone.Layout.configure({
      manage: true,
      fetchTemplate: function(path) {
        var done, err, source;
        if (app.JST[path]) {
          return app.JST;
        }
        try {
          source = $(path);
          if (source.length > 0) {
            return app.JST[path] = _.template(source.html());
          } else {
            throw new Error('template not located');
          }
        } catch (_error) {
          err = _error;
          done = this.async();
          $.get("" + app.root + app.templates + path + ".html" + app.cache_bust, function(contents) {
            done(app.JST[path] = _.template(contents));
          }, 'text');
        }
      }
    });
    Backbone.ajax = function() {
      if (arguments[0].url.match(/^(?:(ht|f)tp(s?)\:\/\/)?/)[0] === '') {
        arguments[0].url = arguments[0].url.replace(app.root_regex, app.root + '$&');
      }
      return Backbone.$.ajax.apply(Backbone.$, arguments);
    };
    return app;
  });

}).call(this);
