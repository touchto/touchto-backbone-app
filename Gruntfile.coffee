module.exports = ->
    "use strict"

    # load local tasks
    @loadTasks 'build/tasks'

    # Development is the default
    @registerTask 'default', [
        'coffee:compile'
        'watch'
    ]

    @registerTask 'build', [
        'coffee:compile'
    ]

    return
