module.exports = ->
    "use strict"
    @loadNpmTasks 'grunt-contrib-watch'
    @config 'watch',
        scripts:
            files: ['src/**/*.coffee']
            tasks: ['coffee:compile']
    return
