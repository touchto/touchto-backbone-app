module.exports = ->
    "use strict"
    @loadNpmTasks 'grunt-contrib-uglify'

    @config 'uglify',
        app:
            files:
                'app.min.js': ['app.js']
    return
