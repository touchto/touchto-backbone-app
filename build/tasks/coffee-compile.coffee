module.exports = ->
    "use strict"
    @loadNpmTasks 'grunt-contrib-coffee'
    @config 'coffee',
        compile:
            options:
                bare: false
            expand: true
            flatten: false
            cwd: 'src'
            src: ['**/*.coffee']
            dest: './'
            ext: '.js'
    return
