// This will be to support node integration
if (typeof define !== 'function') {
    var define = require('amdefine')(module);
    var jasmine_jquery = require('jasmine-jquery');
    var jsdom = require('jsdom');
    var document = jsdom.jsdom('<html><body></body></html>', null);
    var window = document.parentWindow;
    var $ = jQuery = require('jquery')(window);
}

define(['../app'], function(app) {
    describe('App', function() {
        it('Has a name of Untitled Application', function() {
            expect(app.name).toEqual('Untitled Application');
        });
        it('Has a default root of /', function() {
            expect(app.root).toMatch('/');
        });
        it('Has a title of Untitled', function() {
            expect(app.title).toMatch('Untitled');
        });
        it('Can build a module', function() {
            a = app.module()
            expect(a).toBeDefined()
            expect(a.Routes).toBeDefined()
            expect(a.Controllers).toBeDefined()
            expect(a.Models).toBeDefined()
            expect(a.Collections).toBeDefined()
            expect(a.Views).toBeDefined()
        });
        xit('Can set the document title', function() {
            app.name = 'Testing'
            app.title = 'Title'
            expect(document.title).toMatch('Title | Testing')
        });
        describe('Action Handlers', function(){
            if(!('document' in window)) return;
            beforeEach(function(){
                if( !Backbone.History.started ) {
                    Backbone.history.start({
                        root: app.root,
                        pushState: false
                    });
                }
                setFixtures('<a id="localclick" href="/test">Click Test (Local)</a>'
                    +'<a id="externalclick" href="http://google.com">Click Test (External)</a>'
                    +'<a id="bypassclick" href="/no-go" data-bypass>Click Test (Bypass)</a>'
                );
            });
            it('Can trap clicks on local links', function() {
                $('#localclick').trigger(app.click);
                expect(Backbone.history.fragment).toMatch('test');
            });
            it('Does not trap clicks on external links', function() {
                Backbone.history.navigate('/', true);
                $('#externalclick').trigger(app.click);
                expect(Backbone.history.fragment).toMatch('');
            });
            it('Does not trap bypass links', function() {
                Backbone.history.navigate('/', true);
                $('#bypassclick').trigger(app.click);
                expect(Backbone.history.fragment).toMatch('');
            });
        });
    });
});
