((window, factory) ->
    "use strict"

    # AMD. Register as an anonymous module.
    # Wrap in function so we have access
    # to root via `this`.
    if typeof define is 'function' and define.amd
        define [
            'backbone'
            'underscore'
            'jquery'
            'backbone.layoutmanager'
        ], ->
            factory.apply window, arguments

    # Node. Does not work with strict CommonJS, but only CommonJS-like
    # environments that support module.exports, like Node.
    else if typeof exports is 'object'
        Backbone = require 'backbone'
        _ = require 'underscore'
        Layout = require 'backbone.layoutmanager'

        # In a browserify build, since this is the entry point, Backbone.$
        # is not bound. Ensure that it is.
        Backbone.$ = require 'jquery'
        module.exports = factory.call window, Backbone, _, Backbone.$, Layout

    # Browser globals.
    else
        window.App = factory.call window, window.Backbone, window._, window.Backbone.$
    return
) (if typeof global is "object" then global else @), (Backbone, _, $) ->
    "use strict"

    # Create a reference to the global object. In browsers, it will map
    # to the `window` object; in Node, it will be `global`.
    window = @
    if Function::define is undefined
        Function::define = (prop, desc) ->
            Object.defineProperty @prototype, prop, desc

    class App
        root_regex: new RegExp '', 'i'
        _root = null
        _name = 'Untitled Application'
        _title = 'Untitled'
        _title_template = '<%= title %> | <%= name %>'
        _title_c = _.template _title_template
        _document = if window.document isnt undefined then window.document else {}
        _JST = {}

        constructor: ->
            if 'document' of window
                $(_document).on 'ajaxError', (event, xhr, options) ->
                    # TODO: Provide configration capabilities to send user to login
                    return
            return @

        @define 'root',
            get: ->
                return _root
            set: (val) ->
                _root = val
                root_escaped = _root.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$&')
                @root_regex = new RegExp("^(?!\/)(?!#{root_escaped})(.+)$", 'i')
                return _root
        @define 'title',
            get: ->
                return _title
            set: (val) ->
                _title = val
                setTitle()
                return _title
        @define 'title_template',
            get: ->
                return _title_template
            set: (val) ->
                _title_template = val
                _title_c = _.template _title_template
                setTitle()
                return _title_template
        @define 'name',
            get: ->
                return _name
            set: (val) ->
                _name = val
                setTitle()
                return _name
        setTitle = ->
            title = _title_c
                title: _title
                name: _name
            _document.title = unescape title
            return

        cache_bust: '?v=' + Math.random()

        module: (additional_properties) ->
            _.extend
                Routes: {}
                Controllers: {}
                Models: {}
                Collections: {}
                Views: {}
                additional_properties
        click: if Modernizr?.touch then 'touchend' else 'click'
        useLayout: (options) ->
            options = _.extend {}, el: 'body', options
            layout = new Backbone.Layout options
            @layout = layout
        JST: _JST
    app = _.extend new App(), Backbone.Events
    if app.root is null then app.root = '/'

    # Application auto register clicks on internal URLs
    action = (event) ->
        # if dragging then don't navigate
        if @dragging
            delete @dragging
            return

        # if element is a dropdown-toggle bail
        if $(@).hasClass('dropdown-toggle') then return true

        # The link tag
        self = $(@)
        root = "#{location.protocol}//#{location.host}#{app.root}"
        
        href =
            prop: self.prop 'href'
            attr: self.attr('href').replace root.substr(0, root.length - 1), ''

        if href.prop.slice(0, root.length) is root
            # Its a local URL, lets go there!
            event.preventDefault and event.preventDefault()
            Backbone.history.navigate href.attr, true

            # If the click happened on a menu item, close the menu
            if $(@).attr('role') is 'menuitem'
                $('.dropdown-toggle', $(@).parents('.dropdown')).trigger 'click'
                if $(@).parents('.navbar-collapse.in').length > 0
                    $('.navbar-toggle', $(@).parents('.navbar')).trigger 'click'
            return false
        return

    if 'document' of window
        # Get all the application clicks
        action_selector = 'a[href]:not([data-bypass]):not([href=\\#])'
        $(document).on app.click, action_selector, action

        # If we have a touch based action, track dragging
        if app.click is 'touchend'
            $(document).on 'touchmove', action_selector, (event) ->
                @dragging = true
                return

    Backbone.Layout.configure
        manage: true
        fetchTemplate: (path) ->
            if app.JST[path] then return app.JST
            try
                source = $ path
                if source.length > 0
                    return app.JST[path] = _.template source.html()
                else throw new Error 'template not located'
            catch err
                done = @async()
                $.get "#{app.root}#{app.templates}#{path}.html#{app.cache_bust}", (contents) ->
                    done app.JST[path] = _.template(contents)
                    return
                , 'text'
            return
    Backbone.ajax = ->
        if arguments[0].url.match(/^(?:(ht|f)tp(s?)\:\/\/)?/)[0] is ''
            arguments[0].url = arguments[0].url.replace(app.root_regex, app.root + '$&')
        return Backbone.$.ajax.apply( Backbone.$, arguments )

    # Return a Singleton class instance
    return app
